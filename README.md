# aws-codebuild-docker
This repository contains code for creating a [Docker](https://www.docker.com/) build project in [AWS CodeBuild](https://aws.amazon.com/codebuild/).

A single CodeBuild project using the `buildpsec.yml` file can be used to build multiple Docker images as override environment variables can be passed for `IMAGE_REPO_NAME` and `IMAGE_TAG`. The built image is then pushed to an [Amazon Elastic Container Registry](https://aws.amazon.com/ecr/) repository.

The included `Dockerfile` is a simple example, installing [Chef Workstation](https://downloads.chef.io/chef-workstation/).

This repository can be used together with [colmmg/terraform/codebuild-docker-jenkins](https://gitlab.com/colmmg/terraform/codebuild-docker-jenkins) to setup a [Jenkins](https://jenkins.io/) job to invoke the CodeBuild project to build the Docker image.
